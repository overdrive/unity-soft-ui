import React, { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import history from './utils/history';
import Routes from 'routes';
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import theme from "assets/theme";

function App() {
    return (
        <BrowserRouter history={history}>
            <StyledEngineProvider injectFirst>
                <ThemeProvider theme={theme}>
                    <CssBaseline />
                    <Routes />
                </ThemeProvider>
            </StyledEngineProvider>
        </BrowserRouter>
    );
}

export default App;
