import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import BTable from 'react-bootstrap/Table';
import { useTable } from 'react-table'
import makeData from './makeData'
import unityRequests from './unityRequests';
import {Link} from 'react-router-dom';
import {
	Button,
	Container,
	Row,
	Col,
  } from "react-bootstrap";

function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const { getTableProps, headerGroups, rows, prepareRow } = useTable({
    columns,
    data,
  })

  // Render the UI for your table
  return (
    <BTable striped bordered hover size="sm" {...getTableProps()}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()}>
                {column.render('Header')}
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody>
        {rows.map((row, i) => {
          prepareRow(row)
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                return (
                  <td {...cell.getCellProps()}>
                    {cell.render('Cell')}
                  </td>
                )
              })}
            </tr>
          )
        })}
      </tbody>
    </BTable>
  )
}

function Users() {
	const [users, setUsers] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [isLoggedin, setIsLoggedin] = useState(false)

	useEffect(() => {
		async function onLoad() {
		  try {
        const {body} = await unityRequests(`/unity/getusers/1`);
        setUsers(JSON.parse(body));
		  } catch (e) {
			  console.log(e);
		  }
		  setIsLoading(false);
		}

		onLoad();
	  }, isLoggedin);

  const columns = React.useMemo(
    () => [
      {
        Header: 'Personal',
        columns: [
          {
            Header: 'Name',
            accessor: 'strUser',
          },
          {
            Header: 'Contact',
            accessor: 'strTel',
          },
        ],
      },
      {
        Header: 'Info',
        columns: [
          {
            Header: 'Job',
            accessor: 'strJobTitle',
          },
          {
            Header: 'Role',
            accessor: 'strRole',
          },
          {
            Header: 'Email',
            accessor: 'strEmail',
          },
          {
            Header: 'Location',
            accessor: 'strLocation',
          },
		  {
			id: 'action',
			width: 128,
			sortable: false,
			Cell: ({ row }) => (
				<div className="flex items-center">
					
					<Link 
						to={{ 
							pathname : "/app/user/"+row.original.UserID,
							data: row.original.UserID
						}} 
					>
						<Button
							variant="primary"
							//onClick={handleClick}
							//disabled={!strEmail.length}
						>
							EDIT
						</Button>
					</Link>

				</div>
			)
		}
        ],
      },
    ],
    []
  )

  const data = React.useMemo(() => makeData(20), [])

  return (
    <Container fluid>
			<Row>
        <Col md="12">
          <Table columns={columns} data={users} />
        </Col>
      </Row>
    </Container>
  )
}

export default Users