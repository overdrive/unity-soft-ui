import React, { memo } from 'react';
import { JumbotronWrapper } from './common';

function TermsAndConditions() {
	return (
		<JumbotronWrapper title="Terms And Conditions" col={{md: '12'}} />
	)
}

export default memo(TermsAndConditions);
