import React, { memo, useState } from 'react';
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap';

import { IconCheckbox } from 'react-icon-checkbox';
import {
	BsFillStarFill,
	BsStar,
	BsExclamationCircleFill,
	BsExclamationCircle
} from 'react-icons/bs';

function Tasklist() {
	const [showAddTask, setShowAddTask] = useState(false);
	const [starChecked, setStarChecked] = useState(false);
	const [checked, setChecked] = useState(false);

	const tasks = [
		{
			id: 1,
			task: 'Sign contract for: What are conference organizers afraid of?',
			description:
				'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia, molestiae quas vel sint commodi repudiandae consequuntur',
			tags: ['New Project']
		},
		{
			id: 2,
			task: 'Work on Language changes for SAWIS',
			description:
				'Provident similique accusantium nemo autem. Veritatis obcaecati tenetur iure eius earum',
			tags: ['Frontend', 'Backend']
		}
	];

	const onCheckBoxClick = (id) => {
		tasks.map((task) => {
			return task.id === id ? setStarChecked(!starChecked) : 'test';
		});
	};

	return (
		<Container fluid>
			<Row>
				<Col xs={12} md={12} lg={12}>
					<Card>
						<Card.Header
							as="h2"
							className="justify-content-between align-items-center"
							style={{ display: 'flex' }}
						>
							Tasks{' '}
							<Button
								variant={showAddTask ? 'danger' : 'info'}
								className="btn-fill"
								onClick={() => setShowAddTask(!showAddTask)}
							>
								{showAddTask ? 'Close' : 'Add Task'}
							</Button>
						</Card.Header>
						<Card.Body>
							{showAddTask && (
								<Form className="mb-3">
									<Form.Group controlId="strTask">
										<Form.Label>Add Task:</Form.Label>
										<Form.Control type="text" />
									</Form.Group>
									<Form.Group controlId="strDescription">
										<Form.Label>Add Description:</Form.Label>
										<Form.Control type="text" />
									</Form.Group>
									<Form.Group controlId="strTags">
										<Form.Label>Add Tags:</Form.Label>
										<Form.Control type="text" />
									</Form.Group>
									<Button variant="info" type="submit" className="btn-fill">
										Save Task
									</Button>
								</Form>
							)}
							{tasks.map((task) => (
								<Card style={{ marginBottom: '5px' }}>
									<Card.Body id={task.id}>
										<Container fluid>
											<Row className="justify-content-between align-items-center">
												<Col
													xs={2}
													md={1}
													lg={1}
													style={{ paddingLeft: '5px' }}
												>
													<input
														type="Checkbox"
														id="chkSelectTask"
														style={{ width: '25px', height: '25px' }}
													/>
												</Col>
												<Col xs={7} md={9} lg={8}>
													<Card.Title
														className="mb-2"
														style={{ fontWeight: 500 }}
													>
														{task.task}
													</Card.Title>
													<Card.Text className="text-muted">
														{task.description}
													</Card.Text>
												</Col>
												<Col
													xs={3}
													md={1}
													lg={3}
													style={{ textAlignLast: 'end' }}
												>
													<IconCheckbox
														checked={starChecked}
														checkedIcon={
															<BsFillStarFill
																style={{
																	width: '25px',
																	height: '25px',
																	color: '#E3DB2D'
																}}
															/>
														}
														uncheckedIcon={
															<BsStar
																style={{ width: '25px', height: '25px' }}
															/>
														}
														onClick={() => setStarChecked(!starChecked)}
													/>
													<IconCheckbox
														checked={checked}
														checkedIcon={
															<BsExclamationCircleFill
																style={{
																	width: '25px',
																	height: '25px',
																	color: 'red'
																}}
															/>
														}
														uncheckedIcon={
															<BsExclamationCircle
																style={{ width: '25px', height: '25px' }}
															/>
														}
														onClick={() => setChecked(!checked)}
													/>
												</Col>
											</Row>
										</Container>
									</Card.Body>
								</Card>
							))}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}

export default memo(Tasklist);
