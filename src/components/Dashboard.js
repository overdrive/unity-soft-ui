import React, { memo, useState, useEffect } from 'react';
import { Card, Grid } from "@mui/material";
import {
    SuiBox,
    SuiTypography,
    Graphs
} from './common';

function Dashboard() {
    const [showPreloader, setShowPreLoader] = useState(false);
    const [graphsArr, setGraphsArr] = useState([]);


    useEffect(() => {
        var onLoad = async () => {
            setShowPreLoader(true);

            let graphSchematics = {
                "result": true,
                "payload": [
                    {
                        "GraphTitle": "Ratio of vehicles",
                        "GraphType": "PieGraph",
                        "GraphX": "vehicleType",
                        "GraphY": "amount",
                        "Data": [
                            {
                                "vehicleType": "Cars",
                                "amount": 2364
                            },
                            {
                                "vehicleType": "Bikes",
                                "amount": 252
                            },
                            {
                                "vehicleType": "Trucks",
                                "amount": 50
                            }
                        ]
                    },
                    {
                        "GraphTitle": "Total Traffic over time",
                        "GraphType": "StackGraph",
                        "GraphX": "dtDate",
                        "GraphY": "",
                        "Data": [
                            {
                                "dtDate": "2021 - 12",
                                "Cars": "101",
                                "Bikes": "240",
                                "Trucks": "40"
                            },
                            {
                                "dtDate": "2022 - 1",
                                "Cars": "3",
                                "Bikes": "45",
                                "Trucks": "23"
                            },
                            {
                                "dtDate": "2022 - 2",
                                "Cars": "45",
                                "Bikes": "37",
                                "Trucks": "57"
                            }
                        ]
                    },
                    {
                        "GraphTitle": "Total Traffic over time",
                        "GraphType": "RadarGraph",
                        "GraphX": "vehicle",
                        "GraphY": "amount",
                        "Data": [
                            {
                                "vehicle": "Cars",
                                "amount": "101",
                            },
                            {
                                "vehicle": "Bikes",
                                "amount": "45",
                            },
                            {
                                "vehicle": "Trucks",
                                "amount": "57"
                            }
                        ]
                    },
                    {
                        "GraphTitle": "Total Cars",
                        "GraphType": "CounterGraph",
                        "Data": 2364
                    },
                    {
                        "GraphTitle": "Total Bikes",
                        "GraphType": "CounterGraph",
                        "Data": 252
                    },
                    {
                        "GraphTitle": "Total Trucks",
                        "GraphType": "CounterGraph",
                        "Data": 50
                    },
                    {
                        "GraphTitle": "Total Pedestrians",
                        "GraphType": "CounterGraph",
                        "Data": 30
                    },
                    {
                        "GraphTitle": "Total Onions",
                        "GraphType": "CounterGraph",
                        "Data": 42
                    },
                    {
                        "GraphTitle": "Total Spaghett",
                        "GraphType": "CounterGraph",
                        "Data": 92
                    }
                ]
            };
            
            setGraphsArr(graphSchematics.payload);
            setShowPreLoader(false);
        }

        onLoad();
    }, []);

    function displayGraph(graph, currentGraphNo){
        return (
            <Grid item xs={12} sm={graph.GraphType !== "CounterGraph" ? 6 : 2} xl={graph.GraphType !== "CounterGraph" ? 6 : 2}>
                <Card>
                    <SuiBox backgroundColor="white" backgroundGradient>
                        <SuiBox p={2}>
                            {graph.GraphType !== "CounterGraph" && (graph.GraphTitle || graph.description) ? (
                                <SuiBox px={graph.description ? 1 : 0} pt={graph.description ? 1 : 0}>
                                    {graph.GraphTitle && (
                                        <SuiBox mb={1}>
                                            <SuiTypography variant="h6">{graph.GraphTitle}</SuiTypography>
                                        </SuiBox>
                                    )}
                                    <SuiBox mb={2}>
                                        <SuiTypography variant="button" fontWeight="regular" textColor="text">
                                            {graph.description}
                                        </SuiTypography>
                                    </SuiBox>
                                </SuiBox>
                            ) : null}
                            <Graphs type={graph.GraphType} title={graph.GraphTitle} data={graph.Data} GraphX={graph.GraphX} GraphY={graph.GraphY}/>
                        </SuiBox>
                    </SuiBox>
                </Card>
            </Grid>
        );
    }

    let displayCounters = [];
    let displayGraphs = [];
    for(let i = 0; i < graphsArr.length; i++) {
        if(graphsArr[i]){
            if(graphsArr[i]["GraphType"] !== "CounterGraph") {
                displayGraphs.push(displayGraph(graphsArr[i], i))
            }
            else {
                displayCounters.push(displayGraph(graphsArr[i], i))
            }
        }
    }

	return (
		<>
            <SuiBox py={3}>
                <SuiBox mb={3}>
                    <Grid id="countersContainer" container spacing={3}>
                        {displayCounters.length > 0 ? (
                            <>
                                {displayCounters.map((graph, graphNo) => {
                                    return(graph);
                                })}
                            </>
                        ) : (<h1>No Graphs Available</h1>)}
                    </Grid>
                </SuiBox>
                <SuiBox mb={3}>
                    <Grid id="graphsContainer" container spacing={3}>
                        {displayGraphs.length > 0 ? (
                            <>
                                {displayGraphs.map((graph, graphNo) => {
                                    return(graph);
                                })}
                            </>
                        ) : (<h1>No Graphs Available</h1>)}
                    </Grid>
                </SuiBox>
            </SuiBox>
		</>
	  );
}

export default memo(Dashboard);
