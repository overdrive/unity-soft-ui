import React, { memo } from 'react';
import {
    SuiBox,
    SuiTypography
} from './common';
import { Card, Grid } from "@mui/material";

function LandingPage() {
	return (
        <Card>
            <SuiBox mb={3}>
                <Grid container spacing={3} alignItems="center">
                    <Grid item>
                        <SuiBox display="flex" flexDirection="column" height="100%" style={{padding: '15px'}}>
                            <SuiTypography variant="h5" fontWeight="bold" gutterBottom>
                                Landing Page
                            </SuiTypography>
                            <SuiBox mb={6}>
                                <SuiTypography variant="body2" textColor="text">
                                    Wow Such landing page. more landing page than you probably expected right?
                                </SuiTypography>
                            </SuiBox>
                        </SuiBox>
                    </Grid>
                </Grid>
            </SuiBox>
        </Card>
	)
}

export default memo(LandingPage);