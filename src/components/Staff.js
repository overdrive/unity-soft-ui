import React, { memo } from 'react';
import { JumbotronWrapper } from './common';

function Staff() {
	return (
		<JumbotronWrapper title="Staff" col={{md: '12'}} />
	)
}

export default memo(Staff);
