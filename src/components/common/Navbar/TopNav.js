import React, { useEffect, useState } from 'react';
import { NavDropdown, Image, Container } from 'react-bootstrap';
import { useHistory, Link } from 'react-router-dom';
import { isLoggedIn } from 'utils';
import { Auth } from 'aws-amplify';
import { useLocalStorage, deleteFromStorage } from '@rehooks/local-storage';
import breakpoints from "../../../assets/theme/base/breakpoints";
import { Icon } from "@mui/material"; //, Container
import { SuiBox, SuiTypography } from '../';
import DefaultNavbarLink from './DefaultNavbarLink';
import DefaultNavbarMobile from './DefaultNavbarMobile';
import logoImg from '../../../assets/images/logo.jpg';
import styles from './TopNavStyle';

function TopNav(props) {
    let transparent = ''; //none
    let light = false;
	let history = useHistory();
	const [userDetails] = useLocalStorage('userDetails', {});
	let [username, setUsername] = useState("");

    const classes = styles({light, transparent});
    const [mobileNavbar, setMobileNavbar] = useState(false);
    const [mobileView, setMobileView] = useState(false);

    const openMobileNavbar = ({ currentTarget }) => setMobileNavbar(currentTarget.parentNode);
    const closeMobileNavbar = () => setMobileNavbar(false);

    useEffect(() => {
        function displayMobileNavbar() {
            if (window.innerWidth < breakpoints.values.lg) {
                setMobileView(true);
                setMobileNavbar(false);
            } else {
                setMobileView(false);
                setMobileNavbar(false);
            }
        }
        
        window.addEventListener("resize", displayMobileNavbar);
        displayMobileNavbar();
        return () => window.removeEventListener("resize", displayMobileNavbar);
    }, []);

	useEffect(() => {
        if(isLoggedIn()){
            setUsername(userDetails.data.strName)
        }
	}, [userDetails]);

	async function handleLogout() {
		try {
			await Auth.signOut();
			deleteFromStorage('roles');
			deleteFromStorage('userDetails');
			history.push('/');
		} catch (error) {
			console.log('error signing out: ', error);
		}
	}

	return (
        <Container>
            <SuiBox
                customClass={classes.defaultNavbar}
                py={1}
                px={{ xs: transparent ? 4 : 5, sm: transparent ? 2 : 5, lg: transparent ? 0 : 5 }}
            >
                <SuiBox component={Link} to="/" py={transparent ? 1.5 : 0.75}>
                    <Image className="affiliatesImg" src={logoImg} style={{paddingRight: '10px', height:'60px'}} fluid/>
                    <SuiTypography variant="button" fontWeight="bold" textColor={light ? "white" : "dark"}>
                        Unity Soft
                    </SuiTypography>
                </SuiBox>
                <SuiBox color="inherit" display={{ xs: "none", lg: "flex" }} m={0} p={0}>
                    {props.routes.map(({ path, title, canNavigate, children, icon, useUsername, ...rest }, routeNo) => {
                        if(useUsername && useUsername === true) {
                            title = username.length > 22 ? username.substr(0, 22)+"...": username;
                        }
                        if (canNavigate && (!rest.isChild || rest.isChild !== true)) {
                            if(children) {
                                return (
                                    <>
                                        <NavDropdown key={path+routeNo} title={title} id="collasible-nav-dropdown">
                                            <span className="material-icons-round
                                                            notranslate
                                                            MuiIcon-root
                                                            MuiIcon-fontSizeInherit
                                                            vertical-middle
                                                            text-secondary
                                                            css-1neevca-MuiIcon-root" 
                                                aria-hidden="true"
                                            >
                                                {typeof icon === "string" ? (
                                                        <Icon>{icon}</Icon>
                                                    ) : (
                                                        icon
                                                )}
                                            </span>
                                            {children.map(({ ...subProperties }) => {
                                                if (subProperties.canNavigate) {
                                                    let isCurrentPage = false;
                                                    if(props.currentPage === subProperties.path) {
                                                        isCurrentPage = true;
                                                    }

                                                    if(subProperties.useUsername && subProperties.useUsername === true) {
                                                        subProperties.title = username.length > 22 ? username.substr(0, 22)+"...": username;
                                                    }

                                                    return (
                                                        <>
                                                            <DefaultNavbarLink
                                                                icon={subProperties.icon}
                                                                name={subProperties.title}
                                                                route={subProperties.path}
                                                                light={light}
                                                                isCurrentPage={isCurrentPage}
                                                            />
                                                            <br />
                                                        </>
                                                    );
                                                }
                                                else {
                                                    return (<></>);
                                                }
                                            })}
                                        </NavDropdown>
                                    </>
                                );
                            }
                            else {
                                let isCurrentPage = false;
                                if(props.currentPage === path) {
                                    isCurrentPage = true;
                                }

                                return (
                                    <DefaultNavbarLink
                                        icon={icon}
                                        name={title}
                                        route={path}
                                        light={light}
                                        isCurrentPage={isCurrentPage}
                                    />
                                );
                            }
                        }
                        else {
                            return <></>;
                        }
                    })}
                    {isLoggedIn() && (
                        <DefaultNavbarLink
                            onClick={handleLogout}
                            name="Logout"
                            light={light}
                        />
					)}
                </SuiBox>
                <SuiBox
                    display={{ xs: "inline-block", lg: "none" }}
                    lineHeight={0}
                    py={1.5}
                    pl={1.5}
                    color="inherit"
                    customClass="cursor-pointer"
                    onClick={openMobileNavbar}
                >
                    <Icon fontSize="medium">{mobileNavbar ? "close" : "menu"}</Icon>
                </SuiBox>
            </SuiBox>
            {mobileView && (
                <DefaultNavbarMobile open={mobileNavbar} close={closeMobileNavbar}>
                    {props.routes.map(({ path, title, canNavigate, children, icon, useUsername, ...rest }, routeNo) => {
                        if(useUsername && useUsername === true) {
                            title = username.length > 22 ? username.substr(0, 22)+"...": username;
                        }
                        if (canNavigate && (!rest.isChild || rest.isChild !== true)) {
                            if(children) {
                                return (
                                    <>
                                        <NavDropdown key={path+routeNo} title={title} id="collasible-nav-dropdown">
                                            {typeof icon === "string" ? (
                                                    <Icon>{icon}</Icon>
                                                ) : (
                                                    icon
                                            )}{" "}{" "}
                                            {children.map(({ ...subProperties }) => {
                                                if (subProperties.canNavigate) {
                                                    let isCurrentPage = false;
                                                    if(props.currentPage === subProperties.path) {
                                                        isCurrentPage = true;
                                                    }

                                                    if(subProperties.useUsername && subProperties.useUsername === true) {
                                                        subProperties.title = username.length > 22 ? username.substr(0, 22)+"...": username;
                                                    }

                                                    return (
                                                        <>
                                                            <DefaultNavbarLink
                                                                icon={subProperties.icon}
                                                                name={subProperties.title}
                                                                route={subProperties.path}
                                                                light={light}
                                                                isCurrentPage={isCurrentPage}
                                                            />
                                                            <br />
                                                        </>
                                                    );
                                                }
                                                else {
                                                    return (<></>);
                                                }
                                            })}
                                        </NavDropdown>
                                    </>
                                );
                            }
                            else {
                                let isCurrentPage = false;
                                if(props.currentPage === path) {
                                    isCurrentPage = true;
                                }

                                return (
                                    <DefaultNavbarLink
                                        icon={icon}
                                        name={title}
                                        route={path}
                                        light={light}
                                        isCurrentPage={isCurrentPage}
                                    />
                                );
                            }
                        }
                        else {
                            return <></>;
                        }
                    })}
                    {isLoggedIn() && (
                        <DefaultNavbarLink
                            onClick={handleLogout}
                            name="Logout"
                            light={light}
                        />
					)}
                </DefaultNavbarMobile>
            )}
        </Container>
	);
}

export default TopNav;
