import React, { memo, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Container } from 'react-bootstrap';
import { isLoggedIn } from 'utils';
import { useLocalStorage } from '@rehooks/local-storage';
import Icon from "@mui/material/Icon";

function Footer(props) {
	const [userDetails] = useLocalStorage('userDetails', {});
	let [username, setUsername] = useState("");

	useEffect(() => {
        if(isLoggedIn()){
            setUsername(userDetails.data.strName);
        }
	}, [userDetails]);
	return (
		<>
			{props.currentPage !== "/" ?(
                <Row className="customFooter w3-padding-large">
                    <Row className="" style={{marginBottom:"30px"}}>
                        <Col md={12} style={{textAlign:'center'}}></Col>
                    </Row>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={6} className="footerHeadingsCols">
                            <span className="footerHeadings">
                                <b>Unity Soft</b>
                            </span>
                            <br />
                            <p className="footerBodyText">Unity soft is so soft, you can spread it on bread without worrying that it will make holes</p>
                        </Col>
                        <Col xs={6} sm={6} md={6} lg={{span:2, offset:1}} className="footerHeadingsCols">
                            <span className="footerHeadings">
                                <b>Links</b>
                            </span>
                            <br />
                            <ul style={{ listStyleType: 'none', paddingLeft: '0px' }}>
                                {props.routes.map(({ path, title, showInFooter, children, icon, useUsername, ...rest }) => {
                                    if(useUsername && useUsername === true) {
                                        title = username.length > 22 ? username.substr(0, 22)+"...": username;
                                    }
                                    if (showInFooter === true) {
                                        let isCurrentPage = false;
                                        if(props.currentPage === path) {
                                            isCurrentPage = true;
                                        }

                                        return (
                                            <li key={path} >
                                                <Link key={path} to={`${path}`} style={{textDecoration: isCurrentPage ? 'underline' : 'none'}} className="footerLinks">
                                                    {typeof icon === "string" ? (
                                                            <Icon>{icon}</Icon>
                                                        ) : (
                                                            icon
                                                    )}{" "}{" "}
                                                    {title}
                                                </Link>
                                            </li>
                                        );
                                    }
                                    else{
                                        return <></>;
                                    }
                                })}
                            </ul>
                        </Col>
                        <Col xs={6} sm={6} md={6} lg={3} className="footerHeadingsCols">
                            <span className="footerHeadings">
                                <b>Contact Us</b>
                            </span>
                            <br />
                            <Row>
                                <Col>
                                    <Row>
                                        <b>Enquiries</b><br/>
                                        <Link to="mailto:helpme@please.com" href="mailto:helpme@please.com">helpme@please.com</Link>
                                    </Row><br/>
                                    <Row>
                                        <b>Other Enquiries</b><br/>
                                        <Link to="mailto:helpme@please.com" href="mailto:helpme@please.com">helpme@please.com</Link>
                                    </Row>
                                </Col>
                                <Col>
                                    <b>Postal Address</b><br/>
                                    21 Butt Street<br/>
                                    Pennsylvania<br/>
                                    USA<br/>
                                    4687<br/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Row>
            ) : (<></>)}
            <Container fluid>
                <Row className="footerCopyrightSection">
                    <Col sm={12} md={12} lg={12}>
                        <p
                            style={{
                                textAlign: 'center',
                                fontSize: '14px',
                                marginTop: '20px'
                            }}
                        >
                            &copy; 2021 Silicon Overdrive. All Rights Reserved{' '}
                        </p>
                    </Col>
                </Row>
            </Container>
		</>
	);
}

export default memo(Footer);
