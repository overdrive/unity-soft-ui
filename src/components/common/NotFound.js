import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';


const NotFound = (props) => (
    <div className="center-to-screen pb-3 pt-3">
        <Container>
            <Row>
                <Col style={{textAlign:'center'}}>
                    <h1>404 not found</h1>
                    <h1>The page you requested could not be found</h1>
                    <Link to="/">Back to home</Link>
                </Col>
            </Row>
        </Container>
    </div>
);

export default memo(NotFound);
