//pulled from https://github.com/AnomalyInnovations/serverless-stack-demo-user-mgmt-client under MIT license
import React from "react";
import { Button, Spinner } from "react-bootstrap";

import "./LoaderButton.css";

export default function LoaderButton({
  isLoading,
  className = "",
  disabled = false,
  ...props
}) {
  return (
    <Button
      className={`${className} LoaderButton`}
      disabled={disabled || isLoading}
      {...props}
    >
      {isLoading ? <Spinner animation="border"/> : props.children}
    </Button>
  );
}