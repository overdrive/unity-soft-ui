import React, { memo , useState, useEffect } from 'react';
import { JumbotronWrapper } from './common';
import { useLocation } from "react-router-dom";
import unityRequests from './unityRequests';
import {
	Badge,
	Button,
	Card,
	Form,
	Navbar,
	Nav,
	Container,
	Row,
	Col,
  } from "react-bootstrap";

function Profile() {
	const [user, setUser] = useState({});
	const [isLoading, setIsLoading] = useState(true);
	const [isLoggedin, setIsLoggedin] = useState(false);
	let [selected, setSelected] = useState([]);
	let location = useLocation();
	let userId = location.data;

	function handleChange(e) {
		const { options } = e.target;
		let selected = [];

		for (let i = 0; i < options.length; i++) {
			if (options[i].selected) {
				selected.push(options[i].value);
			}
		}
		setSelected(selected);
	}

	useEffect(() => {
		async function onLoad() {
		  try {
			let vars = {id:userId};
			const {body} = await unityRequests(`/unity/getdetails`,vars);
			setUser(JSON.parse(body)[0]);
			console.log(user);
		  } catch (e) {
			console.log(e);
		  }
		  setIsLoading(false);
		}

		onLoad();
	  }, [isLoggedin]);


	

	  return (
		<>
		  <Container fluid>
			<Row>
			  <Col md="8">
				<Card>
				  <Card.Header>
					<Card.Title as="h4">Edit Profile</Card.Title>
				  </Card.Header>
				  <Card.Body>
					<Form>
					  <Row>
						<Col className="pr-1" md="5">
						  <Form.Group>
							<label>Company (disabled)</label>
							<Form.Control
							  defaultValue="Silicon Overdrive"
							  disabled
							  placeholder="Company"
							  type="text"
							></Form.Control>
						  </Form.Group>
						</Col>
						<Col className="px-1" md="3">
						  <Form.Group>
							<label>Username</label>
							<Form.Control
							  value= {user.strUser}
							  placeholder="terminator000100101"
							  type="text"
							></Form.Control>
						  </Form.Group>
						</Col>
						<Col className="pl-1" md="4">
						  <Form.Group>
							<label htmlFor="exampleInputEmail1">
							  Email address
							</label>
							<Form.Control
							  value={user.strEmail}
							  placeholder="terminator000100101@skynet.world"
							  type="email"
							></Form.Control>
						  </Form.Group>
						</Col>
					  </Row>
					  <Row>
						<Col className="pr-1" md="4">
						  <Form.Group>
							<label>First Name</label>
							<Form.Control
							  value= {user.strUser}
							  placeholder="Not"
							  type="text"
							></Form.Control>
						  </Form.Group>
						</Col>
						<Col className="px-1" md="4">
						  <Form.Group>
							<label>Last Name</label>
							<Form.Control
							  value= {user.strUser}
							  placeholder="A Robot"
							  type="text"
							></Form.Control>
						  </Form.Group>
						</Col>
						<Col className="pl-1" md="4">
							<Form.Group controlId="exampleForm.ControlSelect1">
								<Form.Label>Select Role:</Form.Label>
								<Form.Control
									as="select"
									value={selected}
									onChange={handleChange}
									multiple
								>
									<option value="SUPER_ADMIN">SUPER_ADMIN</option>
									<option value="ADMIN">ADMIN</option>
									<option value="MANAGER">MANAGER</option>
									<option value="CUSTOMER">CUSTOMER</option>
									<option value="GUEST">GUEST</option>
								</Form.Control>
							</Form.Group>
						</Col>
					  </Row>
					  <Row>
						<Col md="12">
						  <Form.Group>
							<label>Address</label>
							<Form.Control
							  value= {user.strUser}
							  type="text"
							></Form.Control>
						  </Form.Group>
						</Col>
					  </Row>
					  <Row>
						<Col className="pr-1" md="4">
						  <Form.Group>
							<label>City</label>
							<Form.Control
							  value= {user.strLocation}
							  placeholder= {user.strLocation}
							  type="text"
							></Form.Control>
						  </Form.Group>
						</Col>
						<Col className="px-1" md="4">
						  <Form.Group>
							<label>Country</label>
							<Form.Control
							  value= {user.strLocation}
							  placeholder={user.strLocation}
							  type="text"
							></Form.Control>
						  </Form.Group>
						</Col>
						<Col className="pl-1" md="4">
						  <Form.Group>
							<label>Postal Code</label>
							<Form.Control
							  value= {user.strLocation}
							  placeholder={user.strLocation}
							  type="number"
							></Form.Control>
						  </Form.Group>
						</Col>
					  </Row>
					  <Row>
						<Col md="12">
						  <Form.Group>
							<label>About Me</label>
							<Form.Control
							  cols="80"
							  defaultValue="I am definitly a person and not a robot"
							  placeholder="Here can be your description"
							  rows="4"
							  as="textarea"
							></Form.Control>
						  </Form.Group>
						</Col>
					  </Row>
					  <Button
						className="btn-fill pull-right"
						type="submit"
						variant="info"
					  >
						Update Profile
					  </Button>
					  <div className="clearfix"></div>
					</Form>
				  </Card.Body>
				</Card>
			  </Col>
			  <Col md="4">
				<Card className="card-user">
				  <div className="card-image">
					<img
					  alt="..."
					  src={
						require("../assets/images/faces/placeholder.png")
						  .default
					  }
					></img>
				  </div>
				  <Card.Body>
					<div className="author">
					  <a href="#pablo" onClick={(e) => e.preventDefault()}>
						<img
						  alt="..."
						  className="avatar border-gray"
						  src={require("../assets/images/faces/placeholder.png").default}
						></img>
						<h5 className="title">{user.strUser}</h5>
					  </a>
					  <p className="description">{user.strEmail}</p>
					</div>
				  </Card.Body>
				  <hr></hr>
				  <div className="button-container mr-auto ml-auto">
					<Button
					  className="btn-simple btn-icon"
					  href="#pablo"
					  onClick={(e) => e.preventDefault()}
					  variant="link"
					>
					  <i className="fab fa-facebook-square"></i>
					</Button>
					<Button
					  className="btn-simple btn-icon"
					  href="#pablo"
					  onClick={(e) => e.preventDefault()}
					  variant="link"
					>
					  <i className="fab fa-twitter"></i>
					</Button>
					<Button
					  className="btn-simple btn-icon"
					  href="#pablo"
					  onClick={(e) => e.preventDefault()}
					  variant="link"
					>
					  <i className="fab fa-google-plus-square"></i>
					</Button>
				  </div>
				</Card>
			  </Col>
			</Row>
		  </Container>
		</>
	  );
}

export default memo(Profile);
