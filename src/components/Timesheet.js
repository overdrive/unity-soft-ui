import React, { memo } from 'react';
import { JumbotronWrapper } from './common';

function Timesheet() {
	return (
		<JumbotronWrapper title="Timesheet" />
	)
}

export default memo(Timesheet);
