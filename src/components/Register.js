import React, { memo, useState } from 'react';
import { PopupModalContext } from "../contexts/popupModalContext";
import { Form, Button, Col, Modal, Container, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { JumbotronWrapper, LoaderButton } from './common';
import { Auth, API } from 'aws-amplify';
import unityRequests from './unityRequests';
import { BsCheckCircle } from 'react-icons/bs';
import { validateFieldType } from 'utils';


function Register() {
    let { handlePopupModal } = React.useContext(PopupModalContext);
	const [isMultifactor] = useState(true); //If coginto has multifactor-auth enabled, set this to true
	const [form, setForm] = useState({
		"email": "",
		"name": "",
		"surname": "",
        "idNumber": "",
        "idNumberType": "02",
		"password": "",
		"confirmPassword": "",
		"code": "",
        "cellphoneNumber":""
	});
	const [errors, setErrors] = useState({});
	const [isLoading, setIsLoading] = useState(true);
	const [isRegistering, setIsRegistering] = useState(false);
	const [registrationSent, setRegistrationSent] = useState(false);
	const [isSendingCode, setIsSendingCode] = useState(false);
	const [codeSent, setCodeSent] = useState(false);

	const [showTOS, setShowTOS] = useState(false);
	const [showPrivacy, setShowPrivacy] = useState(false);

	const setField = (field, value) => {
		setForm({
			...form,
			[field]: value
		});
		// Check and see if errors exist, and remove them from the error object:
		if(!!errors[field]) {
			setErrors({
				...errors,
				[field]: null
			});
		}
	}

	const getField = (field) => {
		return form[field];
	}

	if(isLoading)
	{
		let emailVerification = window.location.search.substring(1).split('=');
		if(emailVerification[0] === "verify" && emailVerification[0].length > 0)
		{
			setField("email", emailVerification[1]);
			setRegistrationSent(true);
		}
		setIsLoading(false);
	}

	const validateRegistrationForm = () => {
		const {email, name, surname, idNumber, idNumberType, password, confirmPassword, cellphoneNumber} = form;
		const newErrors = {};

		if(!email || email === '') {
			newErrors.email = 'Cannot be blank!';
		}
        else if(!validateFieldType(email, "Email")) {
            newErrors.email = 'Must be valid!';
        }

		if(!name || name === '') {
			newErrors.name = 'Cannot be blank!';
		}
        else if(!validateFieldType(name, "Name")) {
            newErrors.name = 'Must be valid!';
        }

		if(!surname || surname === '') {
			newErrors.surname = 'Cannot be blank!';
		}
        else if(!validateFieldType(surname, "Name")) {
            newErrors.surname = 'Must be valid!';
        }

        if(!cellphoneNumber || cellphoneNumber === '') {
			newErrors.cellphoneNumber = 'Cannot be blank!';
		}
        else if(!validateFieldType(cellphoneNumber, "Phone")) {
            newErrors.cellphoneNumber = 'Must be valid!';
        }

        if(!idNumber || idNumber === '') {
			newErrors.idNumber = 'Cannot be blank!';
		}
        else if(!validateFieldType(idNumber, idNumberType)) {
            newErrors.idNumber = 'Must be valid!';
        }

		if(!password || password === '') {
			newErrors.password = 'Cannot be blank!';
		}

		if(!confirmPassword || confirmPassword === '') {
			newErrors.confirmPassword = 'Cannot be blank!';
		}
		else if(password !== confirmPassword) {
			newErrors.confirmPassword = 'Passwords must match!';
		}

		return newErrors;
	}

	const handleRegistrationFormSubmit = async (e) => {
		e.preventDefault();
		setIsRegistering(true);
		// get our new errors
		const newErrors = validateRegistrationForm();
		// Conditional logic:
		if(Object.keys(newErrors).length > 0) {
			// We got errors!
			setErrors(newErrors);
			setIsRegistering(false);
		}
		else {
			try {
				await Auth.signUp({"username": getField('email'), "password": getField('password'), "attributes": {"email": getField('email')}});
				setIsRegistering(false);
				setRegistrationSent(true);
			}
			catch(error) {
				console.log('Error signing up...', error);
				setIsRegistering(false);
				if(error.code === "UsernameExistsException")
				{
					setErrors({...errors, email: error.message});
				}
				else
				{
					handlePopupModal(error.message)
				}
			}
		}
	}

	function renderRegistrationForm() {
		const handleCloseTOS = () => setShowTOS(false);
  		const handleShowTOS = () => setShowTOS(true);
		const handleClosePrivacy = () => setShowPrivacy(false);
  		const handleShowPrivacy = () => setShowPrivacy(true);
		return (
			<>
				<Modal show={showTOS} onHide={handleCloseTOS}>
					<Modal.Header>
						<Modal.Title>Terms of Service</Modal.Title>
					</Modal.Header>
					<Modal.Body>A whole bunch of terms</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={handleCloseTOS} style={{color: "#1D62F0", borderColor: "#1D62F0"}}>
							Close
						</Button>
					</Modal.Footer>
				</Modal>

				<Modal show={showPrivacy} onHide={handleClosePrivacy}>
					<Modal.Header>
						<Modal.Title>Privacy Policy</Modal.Title>
					</Modal.Header>
					<Modal.Body>A whole bunch of policies</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={handleClosePrivacy} style={{color: "#1D62F0", borderColor: "#1D62F0"}}>
							Close
						</Button>
					</Modal.Footer>
				</Modal>

				<div className="center-to-screen pt-3">
					<Container>
						<Row>
							<Col>
								<Form className="form">
									<h1>Register</h1>
                                    <Row>
										<Col md={6}>
											<Form.Group controlId="formBasicName" >
												<Form.Control
													type="text" 
													placeholder="Name" 
													value={getField('name')} 
													onChange={e => setField('name', e.target.value)}
													isinvalid={ !!errors.name ? "true" : "false"}
												/>
												<Form.Control.Feedback type='invalid'>
													{ errors.name }
												</Form.Control.Feedback>
											</Form.Group>
										</Col>

										<Col md={6}>
											<Form.Group controlId="formBasicSurname" >
												<Form.Control
													type="text" 
													placeholder="Surname" 
													value={getField('surname')} 
													onChange={e => setField('surname', e.target.value)}
													isinvalid={ !!errors.surname ? "true" : "false" }
												/>
												<Form.Control.Feedback type='invalid'>
													{ errors.surname }
												</Form.Control.Feedback>
											</Form.Group>
										</Col>

										<Col md={6}>
											<Form.Group controlId="formBasicIDNumberType" >
												<select
													className="outstandingScreenDropDowns"
													value={getField('idNumberType')} 
													onChange={e => setField('idNumberType', e.target.value)}
													isinvalid={ !!errors.idNumberType  ? "true" : "false" }
                                                    style={{textAlign: "left"}}
												>
													<option key="02" value="02">
														RSA ID Number
													</option>
													<option key="03" value="03">
														Foreign ID Number
													</option>
													<option key="04" value="04">
														Business Registration
													</option>
												</select>
												<Form.Control.Feedback type='invalid'>
													{ errors.idNumberType }
												</Form.Control.Feedback>
											</Form.Group>
										</Col>

										
										<Col md={6}>
											<Form.Group controlId="formBasicIDNumber" >
												<Form.Control
													type="text" 
													placeholder="ID Number" 
													value={getField('idNumber')} 
													onChange={e => setField('idNumber', e.target.value)}
													isinvalid={ !!errors.idNumber  ? "true" : "false" }
												/>
												<Form.Control.Feedback type='invalid'>
													{ errors.idNumber }
												</Form.Control.Feedback>
											</Form.Group>
										</Col>

                                        <Col md={6}>
                                                <Form.Group controlId="formBasicEmail" >
                                                    <Form.Control
                                                        type="email" 
                                                        placeholder="Email" 
                                                        value={getField('email')} 
                                                        onChange={e => setField('email', e.target.value)}
                                                        isinvalid={ !!errors.email  ? "true" : "false" }
                                                    />
                                                    <Form.Control.Feedback type='invalid'>
                                                        { errors.email }
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                        </Col>

                                        <Col md={6}>
                                                <Form.Group controlId="formBasicMobileNumber" >
                                                    <Form.Control
                                                        type="text" 
                                                        placeholder="Cellphone Number" 
                                                        value={getField('cellphoneNumber')} 
                                                        onChange={e => setField('cellphoneNumber', e.target.value)}
                                                        isinvalid={ !!errors.cellphoneNumber  ? "true" : "false" }
                                                    />
                                                    <Form.Control.Feedback type='invalid'>
                                                        { errors.cellphoneNumber }
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                        </Col>

										<Col md={6}>
											<Form.Group controlId="formBasicPassword" >
												<Form.Control
													type="password" 
													placeholder="Password"
													value={getField('password')}
													onChange={e => setField('password', e.target.value)}
													isinvalid={ !!errors.password  ? "true" : "false" }
												/>
												<Form.Control.Feedback type='invalid'>
													{ errors.password }
												</Form.Control.Feedback>
											</Form.Group>
										</Col>

										<Col md={6}>
											<Form.Group controlId="formBasicConfirmPassword" >
												<Form.Control
													type="password"
													placeholder="Confirm Password"
													value={getField('confirmPassword')}
													onChange={e => setField('confirmPassword', e.target.value)}
													isinvalid={ !!errors.confirmPassword ? "true" : "false" }
												/>
												<Form.Control.Feedback type='invalid'>
													{ errors.confirmPassword }
												</Form.Control.Feedback>
											</Form.Group>
										</Col>
										

										<Col md={{span:4, offset:7}}>
											<LoaderButton
											block
											type="submit"
											bssize="large"
											isLoading={isRegistering}
											onClick={handleRegistrationFormSubmit}
											>
												Submit
											</LoaderButton>
										</Col>
										
										<Col md={{span:6, offset:6}}>
											<p className="mt-3">By Clicking “Submit” you agree to <b>Paymyfines.com</b>’s <span onClick={handleShowTOS}>Terms and Conditions</span> and <span onClick={handleShowPrivacy}>Privacy Policy</span></p>
										</Col>

									</Row>
								</Form>
							</Col>
						</Row>
					</Container>
				</div>
			
		</>
		)
	}

	const validateCodeForm = () => {
		const {code} = form;
		const newErrors = {};

		if(!code || code === '') {
			newErrors.code = 'Cannot be blank!';
		}

		return newErrors;
	}

	const handleCodeFormSubmit = async (e) => {
		e.preventDefault();
		setIsSendingCode(true);
		// get our new errors
		const newErrors = validateCodeForm();
		// Conditional logic:
		if(Object.keys(newErrors).length > 0) {
			// We got errors!
			setErrors(newErrors);
			setIsSendingCode(false);
		}
		else {
			try {
				await Auth.confirmSignUp(getField('email'), getField('code').trim());
				let cogintoUserData = await Auth.signIn(getField('email'), getField('password'));
				//const jwtToken = cogintoUserData.signInUserSession.accessToken.jwtToken;
				if(cogintoUserData.attributes.email_verified === true) {
					const userUuid = cogintoUserData.username;
					let uuidRequest = API.get("unity", `/user/details?strCognitoUuid=${userUuid}`);
					let emailRequest = API.get("unity", `/user/details?strEmail=${getField('email')}`);

					Promise.allSettled([uuidRequest, emailRequest]).then(async (results) => {
						var uuidRequestResult = results[0].value;
						var emailRequestResult = results[1].value;
                        
						if(uuidRequestResult.result === false || emailRequestResult.result === false) {
							if(emailRequestResult.payload === "User does not exist") {
                                let newUserData = {
                                    "strCognitoUuid": userUuid,
                                    "strName": getField('name'),
                                    "strSurname": getField('surname'),
                                    "strEmail": getField('email'),
                                    "strIdType": getField('idNumberType'),
                                    "strIdNumber": getField('idNumber'),
                                    "strMobileNumber": getField('cellphoneNumber')
                                };
                                //add a new entry
                                await unityRequests(`/register`, newUserData);
                            }
                            else {
                                //update the users refUserID to match their coginto identity
                                await unityRequests(`/updateregistration`, {"body": {"strCognitoUuid":userUuid, "strEmail": getField('email')}});
                            }
						}
                        setIsSendingCode(false);
						setCodeSent(true);
					});
				}
				else {
					throw new Error("User mail remains unconfirmed");
				}
			}
			catch(error) {
				console.log('Error confirming sign up...', error);
				setIsSendingCode(false);
				handlePopupModal(error.message);
			}
		}
	}

	const handleResendInvite = async (e) => {
		e.preventDefault();
		setIsSendingCode(true);
		try {
			await Auth.resendSignUp(getField('email'));
			setIsSendingCode(false);
			handlePopupModal('New invite sent!');
		}
		catch(error) {
			console.log('Error resending invite...', error);
			setIsSendingCode(false);
			handlePopupModal('Error resending invite');
		}
	}

	function renderCodeVerification()
	{
		return(
			<div className="center-to-screen pb-3 pt-3">
				<Container>
					<Row>
						<Col md={12}>
							<div className="fontGrey">Didn't get an email? Click <span style={{"color": "#007bff"}} onClick={handleResendInvite}>here</span> to resend it.</div>
							<Form className="form">
								<h1>{"Confirm the verification code emailed to '"+getField('email')+"'"}</h1>
								<Form.Group controlId="formBasicEmail" >
									<Form.Label>Verification Code <b>*</b></Form.Label>
									<Form.Control
										type="text" 
										placeholder="Code" 
										value={getField('code')} 
										onChange={e => setField('code', e.target.value)}
										isinvalid={ !!errors.code ? "true" : "false" }
									/>
									<Form.Control.Feedback type='invalid'>
										{ errors.code }
									</Form.Control.Feedback>
								</Form.Group>

                                <Col md={{span:4, offset:4}}>
                                    <LoaderButton
                                        block
                                        type="submit"
                                        bssize="large"
                                        isLoading={isSendingCode}
                                        onClick={handleCodeFormSubmit}
                                    >
                                    Confirm Code
                                    </LoaderButton>
                                </Col>
							</Form>
						</Col>
					</Row>
				</Container>
				
			</div>
		)
	}

	function renderSuccessMessage() {
		return (
			<div className="center-to-screen pb-3 pt-3">
				<Container>
					<Row>
						<Col md={{span:4, offset:4}}>
							<h1>Register</h1>
							<div className="success">
								<p><BsCheckCircle/> Your account registration has been submitted.</p>
								<p>
									<Link className="fontGrey" href="/login" to="/login">
										Click here to login with your new account.
									</Link>
								</p>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}

	return (
		<div className="Register">
			{!isMultifactor
				? !registrationSent ? renderRegistrationForm() : renderSuccessMessage()
				: !registrationSent ? renderRegistrationForm() : !codeSent ? renderCodeVerification() : renderSuccessMessage()
			}
		</div>
	)
}

export default memo(Register);
