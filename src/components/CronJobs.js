import React, { memo } from 'react';
import { JumbotronWrapper } from './common';

function CronJobs() {
	return (
		<JumbotronWrapper title="CronJobs" col={{md: '12'}} />
	)
}

export default memo(CronJobs);
