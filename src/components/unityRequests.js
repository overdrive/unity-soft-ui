import { API , Auth } from "aws-amplify";

async function unityRequests(path,vars = {} , action = "post") { 
	const user = Auth.currentSession();
    const apiName = 'unity';
	let result = {};
	if(action == "get"){
		result = await API.get(apiName, path,vars);//, myInit);
	}else{
		result = await API.post(apiName, path,{body:vars});//, myInit);
	}	
	return result;
}

export default unityRequests;