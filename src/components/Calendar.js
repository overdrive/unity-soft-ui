import React from 'react'
import FullCalendar, { formatDate } from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import unityRequests from './unityRequests';
import { getUserDetails } from 'utils';
import { INITIAL_EVENTS, createEventId } from './event-utils'
import "./Calendar.css"
import { Card, Grid } from "@mui/material";
import {
    SuiBox,
    SuiTypography
} from './common';

export default class Calendar extends React.Component {
  calendarRef = React.createRef()
  state = {
    isLoading: false,
    weekendsVisible: true,
    currentEvents: []
  }

  render() {
    return (
        <div className='calendar-app'>
            <SuiBox mb={3}>
                <Grid container spacing={3}>
                    <Grid item xl={4}>
                        <Card style={{height: '100%'}}>
                            {this.renderSidebar()}
                        </Card>
                    </Grid>
                    <Grid item xl={8}>
                        <Card>
                            <div className='calendar-app-main'>
                                <FullCalendar
                                    ref={this.calendarRef}
                                    plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                                    headerToolbar={{
                                    left: 'prev,next today',
                                    center: 'title',
                                    right: 'dayGridMonth,timeGridWeek,timeGridDay'
                                    }}
                                    initialView='dayGridMonth'
                                    editable={true}
                                    selectable={true}
                                    selectMirror={true}
                                    dayMaxEvents={true}
                                    datesSet={this.handleDates}
                                    weekends={this.state.weekendsVisible}
                                    events={this.state.currentEvents} // alternatively, use the `events` setting to fetch from a feed
                                    select={this.handleDateSelect}
                                    eventContent={renderEventContent} // custom render function
                                    //eventsSet={this.handleEvents} // called after events are initialized/added/changed/removed
                                    // you can update a remote database when these fire:
                                    eventClick={this.handleEventClick}
                                    eventAdd={this.handleEventAdd}
                                    eventChange={this.handleEventChange} // called for drag-n-drop/resize
                                    eventRemove={this.handleEventRemove}
                                />
                            </div>
                        </Card>
                    </Grid>
                </Grid>
            </SuiBox>
        </div>
    )
  }

  renderSidebar() {
    return (
      <div className='calendar-app-sidebar' style={{height: '100%'}}>
        <div className='calendar-app-sidebar-section'>
          <h2>Instructions</h2>
          <ul>
            <li>Select dates and you will be prompted to create a new event</li>
            <li>Drag, drop, and resize events</li>
            <li>Click an event to delete it</li>
          </ul>
        </div>
        <div className='calendar-app-sidebar-section'>
          <label>
            <input
              type='checkbox'
              checked={this.state.weekendsVisible}
              onChange={this.handleWeekendsToggle}
            ></input>
            toggle weekends
          </label>
        </div>
        <div className='calendar-app-sidebar-section'>
          <h2>All Events ({this.state.currentEvents.length})</h2>
          <ul>
            {this.state.currentEvents.map(renderSidebarEvent)}
          </ul>
        </div>
      </div>
    )
  }

  transformToDB = (item) => {
    let newItem = {};
    newItem['CalendarIDitem'] = item.id;
    newItem['txtDescription'] = item.title;
    newItem['txtNotes'] = item.description;
    newItem['dtStartDate'] = item.start;
    newItem['dtEndDate'] = item.end;
    newItem['strFirstUser'] = item.strFirstUser;
    newItem['refSysUserID'] = item.refSysUserID;
    return newItem;
  }

  transformToReact = (item) => {
    let newItem = {};
    newItem['id'] = item.CalendarIDitem;
    newItem['title'] = item.txtDescription;
    newItem['description'] = item.txtNotes;
    newItem['start'] = item.dtStartDate;
    newItem['end'] = item.dtEndDate;
    newItem['strFirstUser'] = item.strFirstUser;
    newItem['refSysUserID'] = item.refSysUserID;
    return newItem;
  }

  handleDates = async (rangeInfo) => {
    this.setState({
      ...this.state,
      isLoading: true
    });
    try {
      const {body} = await unityRequests(`/unity/getcalendarevents/`, {userID: getUserDetails().userID, dtStartDate: rangeInfo.startStr, dtEndDate: rangeInfo.endStr});
      this.setState({
        ...this.state,
        currentEvents: JSON.parse(body).map(this.transformToReact),
        isLoading: false
      });
    } catch (e) {
      console.log(e);
      this.setState({
        ...this.state,
        isLoading: false
      });
    }
  }

  handleWeekendsToggle = () => {
    this.setState({
      ...this.state,
      weekendsVisible: !this.state.weekendsVisible
    })
  }

  handleDateSelect = (selectInfo) => {
    let title = prompt('Please enter a new title for your event')
    let calendarApi = selectInfo.view.calendar

    calendarApi.unselect() // clear date selection

    if (title) {
      calendarApi.addEvent({
        id: createEventId(),
        title,
        start: selectInfo.startStr,
        end: selectInfo.endStr,
        strFirstUser: getUserDetails().userName,
        refSysUserID: getUserDetails().userID,
        allDay: selectInfo.allDay
      })
    }
  }

  handleEventClick = (clickInfo) => {
    if (window.confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
      clickInfo.event.remove()
    }
  }

  handleEventAdd = async (addInfo) => {
    this.setState({
      ...this.state,
      isLoading: true
    });
    try {
      const {body} = await unityRequests(`/unity/addcalendarevent/`, this.transformToDB(addInfo.event.toPlainObject()));
      addInfo.event.id = body.id;
      this.setState({
        ...this.state,
        isLoading: false
      });
    } catch (e) {
      console.log(e);
      addInfo.revert();
      this.setState({
        ...this.state,
        isLoading: false
      });
    }
  }

  handleEventChange = async (changeInfo) => {
    this.setState({
      ...this.state,
      isLoading: true
    });
    try {
      const {body} = await unityRequests(`/unity/updatecalendarevent/`, this.transformToDB(changeInfo.event.toPlainObject()));
      this.setState({
        ...this.state,
        isLoading: false
      });
    } catch (e) {
      console.log(e);
      changeInfo.revert();
      this.setState({
        ...this.state,
        isLoading: false
      });
    }
  }

  handleEventRemove = async (removeInfo) => {
    this.setState({
      ...this.state,
      isLoading: true
    });
    try {
      const {body} = await unityRequests(`/unity/removecalendarevent/`, this.transformToDB(removeInfo.event.toPlainObject()));
      this.setState({
        ...this.state,
        isLoading: false
      });
    } catch (e) {
      console.log(e);
      removeInfo.revert();
      this.setState({
        ...this.state,
        isLoading: false
      });
    }
  }
}

function renderEventContent(eventInfo) {
  return (
    <>
      <b>{eventInfo.timeText}</b>
      <i>{eventInfo.event.title}</i>
    </>
  )
}

function renderSidebarEvent(event) {
  return (
    <li key={event.id}>
      <b>{formatDate(event.start, {year: 'numeric', month: 'short', day: 'numeric'})}</b>
      <i>{event.title}</i>
    </li>
  )
}

