export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    s3: {
      REGION: "eu-west-1",
      BUCKET: "unity-lite-files"
    },
    apiGateway: {
      REGION: "eu-west-1",
      URL: "https://2dg2tax5pg.execute-api.eu-west-1.amazonaws.com/prod"
    },
    cognito: {
      REGION: "eu-west-1",
      USER_POOL_ID: "eu-west-1_qAilrD6X0",
      APP_CLIENT_ID: "66ajgtvc57ig3rh5hivfmat2a",
      IDENTITY_POOL_ID: "eu-west-1:f4892151-eb6c-4e57-8b44-971cf1b1ad65"
    }
  };