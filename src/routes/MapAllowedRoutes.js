import React, { memo, useEffect } from 'react';
import { Switch, Route, useLocation } from 'react-router-dom';
import { TopNav, Footer, SuiPageLayout, NotFound } from 'components/common';
import { PopupModalProvider } from "../contexts/popupModalContext";

/*
* This is the route utility component used for generating
* routes and child routes it only requires routes array and basePath
*/
function MapAllowedRoutes({routes}) {
    let location = useLocation();

    useEffect(() => {
        document.documentElement.scrollTop = 0;
        document.scrollingElement.scrollTop = 0;
        if(location.pathname !== "/"){
            document.getElementsByClassName("PageContents")[0].classList.remove("shortFooter");
        }
        else {
            document.getElementsByClassName("PageContents")[0].classList.add("shortFooter");
        }
      }, [location.pathname]);

    //append child routes to enable them
    for(let i = 0; i < routes.length; i++){
        if(routes[i].children){
            routes[i].children.forEach((childRoute) => {
                childRoute.isChild = true;
                routes.push(childRoute);
            });
        }
    }

	return (
        <PopupModalProvider>
            <SuiPageLayout>
                <div className="PageContainer">
                    <div className="TopNav">
                        <TopNav routes={routes} currentPage={location.pathname} className="bg-white" />
                    </div>
                    <div className="PageContents">
                        <Switch>
                            {routes.map((route) => {
                                /*
                                * some variables are used by below code
                                * some are not used by the code but destructure due to remove from rest object
                                * just make sure that rest object only contain props that supported by react-router's route component
                                * you may find props list here https://reactrouter.com/web/api/Route
                                */
                                const { path, component: Component, title, permission, canNavigate, showInFooter, ...rest } = route;

                                return (
                                    <Route {...rest} exact key={path} path={`${path}`}>
                                        <Component/>
                                    </Route>
                                )
                            })}
                            <Route><NotFound/></Route>
                        </Switch>
                    </div>
                    <div className="footer">
                        <Footer routes={routes} currentPage={location.pathname}/>
                    </div>
                </div>
            </SuiPageLayout>
        </PopupModalProvider>
	)
}

export default memo(MapAllowedRoutes);
