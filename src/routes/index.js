import React from 'react';
import { getAllowedRoutes } from 'utils';
import { RoutingConfig } from 'config';
import MapAllowedRoutes from 'routes/MapAllowedRoutes';

function Routing() {
	let allowedRoutes = getAllowedRoutes(RoutingConfig);
    
	return (
		<MapAllowedRoutes routes={allowedRoutes} basePath="/"/>
	);
}
export default Routing;


