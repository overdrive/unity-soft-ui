import { Roles } from 'config'

// Components
import {
    LandingPage,
	Calendar,
	Tasklist,
	Users,
	Configer,
	Staff,
	CronJobs,
	Dashboard,
	Profile,
    Login,
	Register,
	ForgotPassword,
    TermsAndConditions
} from 'components';
import PivotTable from 'components/PivotTable';
import { FaUser } from 'react-icons/fa';



// TODO:
/*
* 1. Make title optional
* 2. Make title multi type support ie: (string, node, react element)
* 3. Add child route support
* */

var routes = [
    {
		component: LandingPage,
		path: '/',
		title: 'Home',
		canNavigate: true,
        showInFooter: true,
        permission: [Roles.SUPER_ADMIN, Roles.ADMIN, Roles.MANAGER, Roles.CUSTOMER, Roles.GUEST]
	},
	{
		component: Calendar,
		path: '/calendar',
		title: 'Calendar',
		canNavigate: true,
        showInFooter: true,
        permission: [Roles.SUPER_ADMIN, Roles.ADMIN, Roles.MANAGER, Roles.CUSTOMER, Roles.GUEST]
	},
	{
		component: Tasklist,
		path: '/tasklist',
		title: 'Tasklist',
		canNavigate: true,
        showInFooter: true,
        permission: [Roles.SUPER_ADMIN, Roles.ADMIN, Roles.MANAGER, Roles.CUSTOMER, Roles.GUEST]
	},
	{
		component: Users,
		path: '/users',
		title: 'Users',
		canNavigate: true,
        showInFooter: true,
        permission: [Roles.SUPER_ADMIN, Roles.ADMIN, Roles.MANAGER, Roles.CUSTOMER, Roles.GUEST]
	},
	{
		component: Configer,
		path: '/configer',
		title: 'Config',
		permission: [
            Roles.SUPER_ADMIN, 
            Roles.ADMIN, 
            Roles.MANAGER, 
            Roles.CUSTOMER, 
            Roles.GUEST
        ],
		canNavigate: true,
        showInFooter: false,
		children: [
			{
				component: Staff,
				path: '/staff',
				title: 'Staff',
                canNavigate: true,
                showInFooter: false,
                permission: [Roles.SUPER_ADMIN, Roles.ADMIN, Roles.MANAGER, Roles.CUSTOMER, Roles.GUEST]
			},
			{
				component: CronJobs,
				path: '/cronjobs',
				title: 'Cron Jobs',
                canNavigate: true,
                showInFooter: false,
                permission: [Roles.SUPER_ADMIN, Roles.ADMIN, Roles.MANAGER, Roles.CUSTOMER, Roles.GUEST]
			}
		]
	},
	{
		component: Dashboard,
		path: '/dashboard',
		title: 'Dashboard',
		canNavigate: true,
        showInFooter: true,
		permission: [Roles.SUPER_ADMIN, Roles.ADMIN, Roles.MANAGER, Roles.CUSTOMER, Roles.GUEST],
	},
	{
		component: Profile,
		path: '/profile',
		title: 'Profile',
        icon: <FaUser />,
        useUsername: true,
		canNavigate: true,
        showInFooter: false,
		permission: [
			Roles.SUPER_ADMIN,
			Roles.ADMIN,
			Roles.MANAGER,
			Roles.CUSTOMER
		],
	},
    {
		component: TermsAndConditions,
		path: '/termsAndConditions',
		title: 'Terms and Conditions',
		canNavigate: false,
        showInFooter: true,
		permission: [
			Roles.SUPER_ADMIN,
			Roles.ADMIN,
			Roles.MANAGER,
			Roles.CUSTOMER,
            Roles.GUEST
		],
	},
	{
		component: Login,
		path: '/login',
		title: 'Login',
		permission: [Roles.GUEST],
		canNavigate: true,
        showInFooter: false
	},
	{
		component: ForgotPassword,
		path: '/forgot-password',
		title: 'Forgot Password',
		permission: [Roles.GUEST],
		canNavigate: false,
        showInFooter: false
	},
	{
		component: Register,
		path: '/register',
		title: 'Register',
		permission: [Roles.GUEST],
		canNavigate: false,
        showInFooter: false
	},
]
export default routes;